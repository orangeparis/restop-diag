import netifaces
from netifaces import  AF_INET

import nmap

import serial
from serial.tools import list_ports


class Host(object):
    """
        scan host for

        * local ip interfaces
        * local gateways
        * local ip addresses
        * local serial ports
        * ip neighborhood

    """
    def __init__(self):
        """


        """

        self.local_ip= None
        self.default_gateway= None
        self.local_board= {}

        self.nmap= nmap.PortScanner()
        self.neighborhood= {}
        self._services={}

        self._serial_ports={}

        self._setup()


    def _setup(self):
        """

        :return:
        """
        # setup lan_board
        self.scan_lan()



    @property
    def interfaces(self):
        """

        :return: list of all lan interface names
        """
        return netifaces.interfaces()

    @property
    def gateways(self):
        """

        :return: list of all gateways
        """
        return netifaces.gateways()


    def scan_lan(self):
        """
            scan all local ip on all interfaces

        :return:
        """
        for ifaceName in netifaces.interfaces():
            #print ifaceName
            for ip in netifaces.ifaddresses(ifaceName).setdefault(AF_INET,[{'addr': None}]):
                #print ip
                if ip['addr']:
                    if ip['addr'].startswith('192.168.1.'):
                        self.local_ip = ip['addr']
                    if not ip['addr'] == '127.0.0.1':
                        ip['iface']= ifaceName
                        self.local_board[ip['addr']]= ip

        return self.local_board

    def external_ips(self):
        """

        :return: list
        """
        excluded= ['127.0.0.1']
        return [ip for ip in self.local_board.keys() if ip not in excluded
                and not ip.startswith('192.168.')]

    @property
    def local_ips(self):
        """


        :return: list : all ip on all interfaces
        """
        return self.local_board.keys()


    def scan_hosts(self,hosts='192.168.1.0/24' ):
        """


        :return:
        """
        self.nmap.scan(hosts=hosts, arguments='-n -sP -PE -PA21,23,80,3389')
        hosts_list = [(x, self.nmap[x]['status']['state']) for x in self.nmap.all_hosts()]
        for host, status in hosts_list:
            self.neighborhood[host]= status
            print('{0}:{1}'.format(host, status))
        return hosts_list


    def scan_services(self,host=None,range='22-443'):
        """

        :param hosst:
        :param range:
        :return:
        """
        host= host or self.local_ip
        self._services[host] = self.nmap.scan(host, range)
        return self._services[host]['scan'][host]


    def serial_ports(self):
        """

        :return:
        """
        ports = list_ports.comports()

        for port in ports:
            self._serial_ports[port.device]= port
        rc = [port for port in serial.tools.list_ports.comports() if port[2] != 'n/a']
        return rc


if __name__== "__main__":

    host= Host()

    print  host.interfaces
    print host.gateways

    r= host.local_ips

    r= host.external_ips()

    # for ifaceName in netifaces.interfaces():
    #     addresses = [i['addr'] for i in netifaces.ifaddresses(ifaceName).setdefault(AF_INET, [{'addr': 'No IP addr'}])]
    #     print '%s: %s' % (ifaceName, ', '.join(addresses))
    #
    # print netifaces.interfaces()
    #
    # print netifaces.gateways()

    r= host.scan_hosts()
    print r

    r= host.scan_services(range='5000-5016')
    print r

    r= host.serial_ports()
    print r

    print "Done"