
import random
from scanhost import Host
import requests
import serial
import time


import logging
log= logging.getLogger(__name__)


class RestopStation(Host):
    """



    """
    tvbox_remote_port= 8080
    tvbox_remote_status_url= '/BasicDeviceDescription.xml'
    tvbox_bauds= 115200

    def tvbox_remote_search(self):
        """


        :return:
        """
        candidates=[]
        tvboxes= {}
        # get neighborhood
        hosts= self.scan_hosts()
        # search port 8080 on all neighbourhood
        for host,status in hosts:
            if status == 'up' and host != '192.168.1.1':
                services= self.scan_services(host=host,range='%d-%d' % (self.tvbox_remote_port,self.tvbox_remote_port))
                try:
                    service= services['tcp'][self.tvbox_remote_port]
                    if service['state'] == 'open':
                        # port 8080 is open
                        candidates.append(host)
                except KeyError:
                    pass
                continue
        for candidate in candidates:
            xml= self.tvbox_remote_get_status(candidate)
            if xml:
                tvboxes[candidate]= xml


        return tvboxes

    def tvbox_remote_get_status(self,host):
        """
            fetch stb status : GET http:/host:8080/BasicDeviceDescription.xml
        :param host:
        :return: string xml description or None
        """
        #print host
        url="http://%s:%d%s" % (host,self.tvbox_remote_port,self.tvbox_remote_status_url)
        response= requests.get(url,timeout=3)
        if response.status_code== 200:
            return response.text
        return None


    def tvbox_console_search(self,remote_url=None):
        """

        :return:
        """
        tvbox_console= []
        serial_ports= self.serial_ports()
        for device in serial_ports:
            if self.tvbox_console_sync(device):
                tvbox_console.append(device)
        return tvbox_console

    def tvbox_console_sync(self,device,timeout=10):
        """

        :param device: string ( eg /dev/ttyUSB0
        :return:
        """
        bauds = self.tvbox_bauds
        port = serial.Serial(
            port=device, baudrate=bauds, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=1,
            timeout=1
        )
        # send synchro
        ok= False
        seq= random.sample("abcdef0123456789",8)
        port.write('echo "SYNCHRO-%s"' % seq)
        t= time.time()
        t1= t + 10
        while t < t1:
            line= port.readline()
            if seq in line:
                # found synchro
                ok= True
            t=time.time()
        port.close()
        return ok


if __name__=="__main__":


    host= RestopStation()

    services = host.scan_services()

    stb= host.tvbox_remote_search()

    services= host.scan_services()

    consoles= host.tvbox_console_search()

    print "Done"