
import socket


class Host:
    """


    """

    @property
    def local_ips(self):
        """

        :return: list all local ip except( 127 )
        """
        ips= ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")])
        return ips

    @property
    def local_ip(self):
        """

        :return: str: local address on lan 192.168.1
        """
        return [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if ip.startswith("192.168.1.")][0]



# def get_local_ip():
#     """
#
#
#     :return:
#     """
#
#     local_ips= [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if ip.startswith("192.168.1.")]
#     return local_ips[0]
#
#
#
# print([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1])
#
#
# for ip in socket.gethostbyname_ex(socket.gethostname())[2] :
#     print ip
#
#
# print get_local_ip()


host= Host()

print host.local_ip
print host.local_ips